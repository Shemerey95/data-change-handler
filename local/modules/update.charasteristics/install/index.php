<?php
IncludeModuleLangFile(__FILE__);
if (class_exists("update_sinonim")) {
    return;
}

use Bitrix\Main\ModuleManager;
use Bitrix\Main\EventManager;

class update_charasteristics extends CModule
{
    
    public function __construct()
    {
        if (file_exists(__DIR__ . "/version.php")) {
            include_once(__DIR__ . "/version.php");
            $this->MODULE_ID           = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION      = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME         = 'Charasteristics_processor';
            $this->MODULE_DESCRIPTION  = 'Обработчик заполнения поля "Харастеристики" для процессоров из конфигуратора';
            $this->PARTNER_NAME        = '';
            $this->PARTNER_URI         = '';
        }
    }
    
    public function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);
        $this->InstallEvents();
    }
    
    public function InstallEvents()
    {
        EventManager::getInstance()->registerEventHandler(
            "search",
            "BeforeIndex",
            $this->MODULE_ID,
            "Update\Charasteristics\Main",
            "BeforeIndexHandler"
        );
    }
    
    public function DoUninstall()
    {
        $this->UnInstallEvents();
        ModuleManager::unRegisterModule($this->MODULE_ID);
        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);
    }
    
    public function UnInstallEvents()
    {
        EventManager::getInstance()->unRegisterEventHandler(
            "search",
            "BeforeIndex",
            $this->MODULE_ID,
            "Update\Charasteristics\Main",
            "BeforeIndexHandler"
        );
    }
}
