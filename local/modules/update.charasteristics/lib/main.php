<?php

namespace Update\Charasteristics;

use Bitrix\Main\Diag\Debug;
use CIBlockElement;
use CModule;

class Main
{
    public function BeforeIndexHandler($arFields)
    {
        if (!CModule::IncludeModule("iblock")) {
            return $arFields;
        }
        
        if ($arFields["MODULE_ID"] == "iblock") {
            
            if (strpos($arFields["TITLE"], 'V4 ') != false) {
                $charasteristic = str_replace("V4 ", "", strstr($arFields["TITLE"], 'V4 '));
            } else {
                preg_match('/\((.+)\)/', $arFields["TITLE"], $character);
                $charasteristic = $character[1];
            }
            
            CIBlockElement::SetPropertyValuesEx(
                $arFields["ITEM_ID"],
                false,
                ["CHARACTERISTICS" => $charasteristic]);
        }
        
        return $arFields;
    }
}